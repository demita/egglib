*******
Authors
*******

**Stéphane De Mita**

Evolutionary biologist (INRAE) at the `Plant Health Institute 
Montpellier <https://umr-phim.cirad.fr/>`_ (France).

**Mathieu Siol**

Evolutionary biologist (INRAE) at the `Agroécologie 
<https://www6.dijon.inrae.fr/umragroecologie>`_ lab (Dijon, France).

**Thomas Coudoux**

Bioinformatician appointed by INRAE at the `Interactions Arbres - 
Micro-organismes <https://mycor.nancy.inra.fr/IAM/>`_ lab (Nancy 
France) from 2016 to 2017, now working at `Soladis <https://www.soladis.com/>`_
(Lyon, France).

**Sébastien Ravel**

Bioinformatician (INRAE) at the `Plant Health Institute Montpellier 
<https://umr-phim.cirad.fr/>`_ (France).
