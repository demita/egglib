        =================== ===================================================================================
        option value
        =================== ===================================================================================
        ``query_loc``       format:``start-stop``
        ``strand``          one of: ``both``, ``minus``, ``plus``
        ``task``            one of: ``blastn`` ``blastn-short`` ``dc-megablast``
        ``evalue``          a float
        ``word_size``       an an integer greater or equal than 4
        ``gapopen``         an an integer
        ``gapextend``       an integer
        ``penalty``         an integer greater or equal than 0
        ``reward``          an integer greater or equal than 0
        ``use_index``       a boolean
        ``index_name``      a string
        ``subject``         a file
        ``subject_loc``     a string
        ``outfmt``          one of: ``0``,``1``,``2``,``3``,``4``,``5``,``6``,``7``,``8``,``9``,``10``,``11``,``12``,``13``,``14``,``15``,``16``,``17``,``18``
        ``show_gis
        ``num_descriptions`` an integer greater or equal than 0
        ``num_alignments``  an integer greater or equal than 0
        ``line_length``     an integer greater or equal than 1
        ``html``            a flag
        ``dust``            a string
        ``filtering_db``    a string 
        ``window_masker_taxid``    an integer
        ``window_masker_db`` a string
        ``soft_masking``    a boolean
        ``lcase_masking     a flag
        ``gilist``          a string
        ``seqidlist``       a string
        ``negative_gilist`` a string
        ``entrez_query``    a string
        ``db_soft_mask``    a string
        ``db_hard_mask``    a string
        ``perc_identity``   a float between 0 and 100
        ``qcov_hsp_perc``   a float between 0 and 100
        ``max_hsps``        an integer greater or equal than 1
        ``culling_limit``   an integer greater or equal than 0
        ``best_hit_overhang``    a float between 0 and 0.5
        ``best_hit_score_edge``    a float between 0 and 0.5  
        ``max_target_seqs`` an integer greater or equal than 1
        ``template_type``   one of: ``coding``, ``coding_and_optimal``, ``optimal``
        ``template_length`` an integer, one of: ``16``, ``18``, ``21``
        ``dbsize``          a flag
        ``searchsp``        an integer greater or equal than 0
        ``sum_stats``       a boolean
        ``import_search_strategy``    a file
        ``export_search_strategy``    a file
        ``xdrop_ungap``    a float
        ``xdrop_gap``      a float
        ``xdrop_gap_final``    a float
        ``no_greedy        a flag
        ``min_raw_gapped_score``    an integer
        ``ungapped         a flag
        ``window_size``    an integer greater or equal than 0
        ``off_diagonal_range``    an integer greater or equal than 0
        ``parse_deflines   a flag
        ``num_threads``    an integer greater or equal than 1
        ``remote           a flag

