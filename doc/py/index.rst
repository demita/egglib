.. _egglib-py:

***********************
EggLib Reference Manual
***********************

The documentation of EggLib is organized in modules. The links below
point to pages describing the content of the different modules, along
with a detailed description of all their classes and methods.

.. raw:: html

    <div><dl class="simple" style="margin-left: 30px;">
        <dt><a class="nicelink" style="padding-right: 15px;" href="egglib.html" }}">egglib</a></dt>
            <dd>Generic data holders directly accessible from the <tt class="xref py py-class docutils literal"><span class="pre">egglib</span></tt> namespace</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="alphabets.html">egglib.alphabets</a></dt>
            <dd>Standard alphabets to describe genetic data</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="random.html">egglib.random</a></dt>
            <dd>Pseudorandom number generator</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="tools.html">egglib.tools</a></dt>
            <dd>Generic tools for processing data</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="io.html">egglib.io</a></dt>
            <dd>Input/output operations</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="stats.html">egglib.stats</a></dt>
            <dd>Diversity statistics</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="coalesce.html">egglib.coalesce</a></dt>
            <dd>Coalescent simulator</dd>
        <dt><a class="nicelink" style="padding-right: 15px;" href="wrappers.html">egglib.wrappers</a></dt>
            <dd>Wrappers of external applications</dd>
    </ul></div>


.. toctree::
    :hidden:

    egglib
    alphabets
    random
    tools
    io
    stats
    coalesce
    wrappers
