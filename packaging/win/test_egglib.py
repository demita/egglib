import egglib
print(dir(egglib))
print(egglib.__path__)
print(egglib.__version__)
m = egglib.coalesce.Simulator(2, num_chrom=(20,20), theta=8.0, migr=1.0)
a = m.simul()
print(a.fasta(alphabet=egglib.alphabets.DNA))
struct = egglib.struct_from_samplesizes((20,20))
cs = egglib.stats.ComputeStats(struct=struct)
cs.add_stats('S', 'D', 'FstWC')
for i in m.iter_simul(4, cs=cs):
    print(i)
