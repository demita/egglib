import os, sys, site, tarfile

if sys.version_info[0] != 3 or sys.version_info[1] < 7:
    sysexit('this installation script requires Python 3.7 or higher')

if len(sys.argv) != 2:
    sys.exit("""
    /----------------------------------------------------------------------\\
    | Usage for installing the egglib module:                              |
    |                                                                      |
    |     python install.py <archive>                                      |
    |                                                                      |
    | If there are several Python installations in your system, you must   |
    | use the one you wish to use egglib with.                             |
    |                                                                      |
    \----------------------------------------------------------------------/
    """)

# get first part of member path (will be stripped)
f = tarfile.open(sys.argv[1], mode='r:gz')
for name in f.getnames():
    if os.path.basename(name) == 'site-packages':
        path = name + '/'
        break
else:
    sys.exit('invalid archive')

# get the content and strip path
members = []
for member in f:
    if member.name.startswith(path) and os.path.basename(member.name) != 'site-packages':
        member.name =  member.name.replace(path, '', 1)
        members.append(member)
    else:
        pass

# find destination location
for path in site.getsitepackages():
    if '{0}.{1}'.format(*sys.version_info) in path and 'site-packages' in path:
        dest = path
        break
else: sys.exit('cannot find site package path')
print('installing to', dest)

# install
f.extractall(path=dest, members=members)
