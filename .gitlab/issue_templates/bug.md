Let us know what happens by filling as many of the following items as
possible.

## What happens

- Summarize here what happens and should not, or what happens
  differently than it should.

## How does it happen

- Ideally, provide a script generating the error, and attach a test
  input file as needed. You test input file might end up in the test
  suite::

    import egglib
    generate_error...

- In many cases, it is important to know the details of your system,
  Python version and possible the version of external libraries or tools
  involved in the bug.

## What is the result

- In case of a Python exception, provide the full traceback. Show us
  whatever is displayed and might be informative.

``error message``

## You can add labels below if you know what module is concerned

/label ~bug

to be chosen from:

~bug or ~unstability
~installation
~egglib
~egglib.tools
~egglib.wrappers
~egglib.io
~egglib.coalesce
~egglib.stats

